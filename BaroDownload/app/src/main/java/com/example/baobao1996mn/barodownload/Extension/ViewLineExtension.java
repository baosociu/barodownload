package com.example.baobao1996mn.barodownload.Extension;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.example.baobao1996mn.barodownload.Fragment.FeedbackFragment;
import com.example.baobao1996mn.barodownload.Fragment.HelpFragment;
import com.example.baobao1996mn.barodownload.Fragment.LibraryFragment;
import com.example.baobao1996mn.barodownload.Fragment.SearchFragment;
import com.example.baobao1996mn.barodownload.R;
import com.example.baobao1996mn.barodownload.Utilities.Global;

/**
 * Created by baobao1996mn on 31/08/2017.
 */

public class ViewLineExtension extends View {

    public ViewLineExtension(Context context) {
        super(context);
        setColorBackground();

    }

    public ViewLineExtension(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setColorBackground();

    }

    public ViewLineExtension(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setColorBackground();
    }



    private void setColorBackground() {
        int id = R.color.barColorWhenSelectedDownload;
        if (Global.currentFragment != null) {
            if (Global.currentFragment instanceof FeedbackFragment)
                id = R.color.barColorWhenSelectedFeedback;
            if (Global.currentFragment instanceof HelpFragment)
                id = R.color.barColorWhenSelectedHelp;
            if (Global.currentFragment instanceof LibraryFragment)
                id = R.color.barColorWhenSelectedLibrary;
            if (Global.currentFragment instanceof SearchFragment)
                id = R.color.barColorWhenSelectedSearch;
        }
        int color = getResources().getColor(id);
        setBackgroundColor(color);
    }

}
