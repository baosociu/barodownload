package com.example.baobao1996mn.barodownload.Utilities;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.example.baobao1996mn.barodownload.Adapter.DownloadTaskRecyclerViewAdapter;
import com.example.baobao1996mn.barodownload.AsyncTask.DownloadAsyncTask;
import com.example.baobao1996mn.barodownload.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by baobao1996mn on 31/08/2017.
 */

public class Global {
    static public final String host = "http://barodownload.000webhostapp.com/download/";

    static public final String OPEN_DOWNLOAD_TASKS = "OPEN_DOWNLOAD_TASKS";
    static public final int ID_ASYNC = 123456;
    static public Fragment currentFragment;
    static public List<DownloadAsyncTask> downloadAsyncTaskList;
    static public DownloadTaskRecyclerViewAdapter downloadTaskRecyclerViewAdapter;

    static public ImageView imageVideo;
    static public Handler handlerIntentService;
    static public Runnable runnableInentService;
    static public NotificationManager notificationManagerDownload;
    static public Notification.Builder builderDownload;

    static public AlertDialog alertDownloadTasks;

    //todo method
    static public void refreshNotification(String fileName) {
        Log.i("DownloadAsyncTask: " + ID_ASYNC + " - " + fileName);
        Global.builderDownload.setProgress(100, 0, false);
        //Global.builderDownload.setSubText(fileName);
        Global.builderDownload.setContentText(fileName);

        Global.notificationManagerDownload.notify(ID_ASYNC, Global.builderDownload.build());
        try {
            //Global.downloadTaskRecyclerViewAdapter.notifyDataSetChanged();
            Global.downloadTaskRecyclerViewAdapter.refreshProgress();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void completeNotification() {
        Global.builderDownload.setContentText("Download complete");
        Global.builderDownload.setProgress(0, 0, false);
        Global.notificationManagerDownload.notify(ID_ASYNC, Global.builderDownload.build());
        try {
            //Global.downloadTaskRecyclerViewAdapter.notifyDataSetChanged();
            Global.downloadTaskRecyclerViewAdapter.completeProgress();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void updateNotification(int p) {
        Global.builderDownload.setProgress(100, p, false);
        Global.notificationManagerDownload.notify(ID_ASYNC, Global.builderDownload.build());
        try {
            Global.downloadTaskRecyclerViewAdapter.updateProgress(Global.downloadAsyncTaskList.get(0).getStringStatus());

            //Global.downloadTaskRecyclerViewAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void createDownloadTasks(Context context) {
        downloadAsyncTaskList = new ArrayList<>();
        Global.downloadTaskRecyclerViewAdapter = new DownloadTaskRecyclerViewAdapter();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_download_tasks, null, false);
        builder.setView(view);
        alertDownloadTasks = builder.create();
    }

    public static void showDownloadTasks(){
        Context context = alertDownloadTasks.getContext();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_download_tasks, null, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerViewDownloadTasks);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 1));
        recyclerView.setAdapter(Global.downloadTaskRecyclerViewAdapter);
        builder.setView(view);
        alertDownloadTasks = builder.create();
        alertDownloadTasks.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Global.alertDownloadTasks.show();
    }
}
