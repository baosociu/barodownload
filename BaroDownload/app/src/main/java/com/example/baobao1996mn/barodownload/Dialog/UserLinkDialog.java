package com.example.baobao1996mn.barodownload.Dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.baobao1996mn.barodownload.R;

/**
 * Created by baobao1996mn on 03/09/2017.
 */

public class UserLinkDialog {
    Context context;
    AlertDialog mAlert ;
    AlertDialog.Builder mBuilder;
    View view;
    String link;
    boolean closed;

    public UserLinkDialog(Context context) {
        this.context = context;
        mBuilder = new AlertDialog.Builder(context);
        view = LayoutInflater.from(context).inflate(R.layout.dialog_get_link,null,false);
        mBuilder.setView(view);
        mAlert = mBuilder.create();
        mAlert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mAlert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                closed = true;
            }
        });

        this.closed = true;
        this.link = "";

        (view.findViewById(R.id.dialogGetLinkButton)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                link = ((EditText)view.findViewById(R.id.dialogGetLinkEditText)).getText().toString();
                link = getLinkDownload(link);
                if(!link.isEmpty())
                    mAlert.cancel();
            }
        });
    }

    public void show(){
        ((EditText)view.findViewById(R.id.dialogGetLinkEditText)).selectAll();
        link = "";
        closed = false;
        mAlert.show();
    }

    public String getLink(){
        return link;
    }

    public boolean isClosed(){
        return closed;
    }

    private String getLinkDownload(String input) {
        //todo check các site hỗ trợ

        //todo check video tồn tại ?

        //todo tạo link
        //return "http://en.mediagetter.com/#url=" + input;
        if (input.startsWith("https://youtu.be/"))
            return "https://www.ssyoutube.com/watch?v=" + input.substring(17);

        if (input.startsWith("https://www.")) {
            return "https://www.ss" + input.substring(12);
        } else
            Toast.makeText(context, "Đường dẫn không hợp lệ", Toast.LENGTH_LONG).show();
        return "";
    }



}
