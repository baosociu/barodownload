package com.example.baobao1996mn.barodownload.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.baobao1996mn.barodownload.Adapter.DownloadRecyclerViewAdapter;
import com.example.baobao1996mn.barodownload.Dialog.UserLinkDialog;
import com.example.baobao1996mn.barodownload.Model.TotalLinkDownload;
import com.example.baobao1996mn.barodownload.Model.WebViewDownload;
import com.example.baobao1996mn.barodownload.R;
import com.example.baobao1996mn.barodownload.Utilities.Global;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DownloadFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public DownloadFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    View view;

    //Todo BindView
    @BindView(R.id.recyclerViewDownload)
    public RecyclerView recyclerView;
    @BindView(R.id.relativeMainDownload)
    public RelativeLayout MAIN_RELATIVE;
    @BindView(R.id.relativeProgressDownload)
    public RelativeLayout PROGRESS_RELATIVE;
    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    @BindView(R.id.collapsing_toolbar)
    public CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.fabDownload)
    public FloatingActionButton fabDownload;
    @BindView(R.id.fabDownloadTasks)
    public FloatingActionButton fabShowTasks;
    @BindView(R.id.fabMenu)
    public FloatingActionMenu menuFab;
    @BindView(R.id.webViewDownload)
    public WebViewDownload webView;
    @BindView(R.id.imgDownload)
    public ImageView imageMain;

    private Unbinder unbinder;
    private UserLinkDialog userLinkDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_download, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onStart() {
        super.onStart();
        settingToolbar();
        settingFab();
        settingWebView();
        modeLoading(false);
    }

    //TODO: Fab
    private void settingFab() {
        userLinkDialog = new UserLinkDialog(this.getContext());
        fabDownload.setColorNormalResId(R.color.barColorWhenSelectedDownload);
        fabDownload.setColorPressedResId(R.color.pressedFab);
        fabShowTasks.setColorNormalResId(R.color.barColorWhenSelectedDownload);
        fabShowTasks.setColorPressedResId(R.color.pressedFab);

        fabDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userLinkDialog.show();
                menuFab.close(true);

                final Handler handler = new Handler();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (userLinkDialog.isClosed()) {
                            if (!userLinkDialog.getLink().isEmpty())
                                loadingLinkDownloadVideo(userLinkDialog.getLink());
                            handler.removeCallbacks(this);
                        } else
                            handler.post(this);
                    }
                });
            }
        });

        fabShowTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Global.showDownloadTasks();

            }
        });
    }

    //todo: tạo những link download video từ url người dùng nhập
    private void loadingLinkDownloadVideo(String linkDownload) {
        modeLoading(true);
        webView.leechLink(linkDownload);
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (webView.isCompletedGetLinkStatus()) {
                    TotalLinkDownload total = webView.getLinkDownload();
                    showDownloadLinks(total);
                    if(total.getSize()!=0)
                        showMainLayout(total.getLinkDownloadAt(0).getTitle(),total.getLinkDownloadAt(0).getImg());
                    modeLoading(false);
                    handler.removeCallbacks(this);
                } else
                    handler.post(this);
            }
        };
        handler.post(runnable);
    }


    //todo toolbar
    private void settingToolbar() {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        collapsingToolbarLayout.setTitle("BaoSoCiu");
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
        int mutedColor = getContext().getResources().getColor(R.color.barColorWhenSelectedDownload);
        collapsingToolbarLayout.setBackgroundColor(mutedColor);

    }

    //todo webview
    private void settingWebView() {
        webView.loadUrl(Global.host);
        webView.getSettings().setJavaScriptEnabled(true);
    }

    //todo image
    private void showMainLayout(String strTitle, String strImg) {
        Picasso.with(this.getContext()).load(strImg).into(imageMain);
        collapsingToolbarLayout.setTitle(strTitle);
    }

    //todo hiển thị các link đã load được dưới dạng RecyclerView
    private void showDownloadLinks(TotalLinkDownload total) {
        DownloadRecyclerViewAdapter adapter = new DownloadRecyclerViewAdapter(total);
        recyclerView.setLayoutManager(new GridLayoutManager(this.getContext(), 1));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    //todo ẩn/hiện processing bar
    private void modeLoading(boolean isLoading) {
        if ((MAIN_RELATIVE.getVisibility() == View.VISIBLE && isLoading) ||
                (MAIN_RELATIVE.getVisibility() == View.INVISIBLE && !isLoading) ||
                (PROGRESS_RELATIVE.getVisibility() == View.INVISIBLE && isLoading) ||
                (PROGRESS_RELATIVE.getVisibility() == View.VISIBLE && !isLoading)) {
            MAIN_RELATIVE.setVisibility(isLoading ? View.INVISIBLE : View.VISIBLE);
            PROGRESS_RELATIVE.setVisibility(isLoading ? View.VISIBLE : View.INVISIBLE);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
