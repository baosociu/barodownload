package com.example.baobao1996mn.barodownload.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by baobao1996mn on 31/08/2017.
 */

public class TotalLinkDownload {
    List<LinkDownload> data;

    public TotalLinkDownload(List<LinkDownload> data){
        this.data = new ArrayList<>(data);
    }

    public List<LinkDownload> getData()
    {
        return data;
    }

    public int getSize(){
        return data.size();
    }

    public LinkDownload getLinkDownloadAt(int position){
        return data.get(position);
    }
}
