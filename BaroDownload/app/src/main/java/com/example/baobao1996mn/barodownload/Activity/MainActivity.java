package com.example.baobao1996mn.barodownload.Activity;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.example.baobao1996mn.barodownload.Fragment.DownloadFragment;
import com.example.baobao1996mn.barodownload.Fragment.FeedbackFragment;
import com.example.baobao1996mn.barodownload.Fragment.HelpFragment;
import com.example.baobao1996mn.barodownload.Fragment.LibraryFragment;
import com.example.baobao1996mn.barodownload.Fragment.SearchFragment;
import com.example.baobao1996mn.barodownload.R;
import com.example.baobao1996mn.barodownload.Utilities.Global;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

public class MainActivity extends AppCompatActivity implements OnTabSelectListener,
        DownloadFragment.OnFragmentInteractionListener,
        SearchFragment.OnFragmentInteractionListener,
        LibraryFragment.OnFragmentInteractionListener,
        FeedbackFragment.OnFragmentInteractionListener,
        HelpFragment.OnFragmentInteractionListener {
    BottomBar bottomBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bottomBar = (BottomBar) findViewById(R.id.bottomBar);

        Global.createDownloadTasks(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        bottomBar.setOnTabSelectListener(this);
        bottomBar.selectTabAtPosition(0);
//        Intent intent = new Intent(this, DownloadService.class);
//        this.startService(intent);
//        getDownloadTasks();
//        openDownloadTasks();
    }

    private void getDownloadTasks() {
        //todo shared preference
    }

    private void openDownloadTasks() {
        boolean open = getIntent().getBooleanExtra(Global.OPEN_DOWNLOAD_TASKS,false);
        if(open){
            Global.showDownloadTasks();
        }
    }




    @Override
    public void onTabSelected(@IdRes int tabId) {
        switch (tabId) {
            case R.id.tab_download:
                Global.currentFragment = new DownloadFragment();
                break;
            case R.id.tab_search:
                Global.currentFragment = new SearchFragment();
                break;
            case R.id.tab_library:
                Global.currentFragment = new LibraryFragment();
                break;
            case R.id.tab_feedback:
                Global.currentFragment = new FeedbackFragment();
                break;
            case R.id.tab_help:
                Global.currentFragment = new HelpFragment();
                break;
        }
        if (Global.currentFragment != null)
            goToFragment(Global.currentFragment);
    }

    private void goToFragment(Fragment next) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.contentMain, next);
        transaction.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    protected void onResume() {
        super.onResume();
    }


}
