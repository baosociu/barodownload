package com.example.baobao1996mn.barodownload.Extension;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by baobao1996mn on 31/08/2017.
 */

public class TextViewExtension extends TextView {


    public TextViewExtension(Context context) {
        super(context);
        setupTypeface();
        setupBackgroud();
    }

    public TextViewExtension(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setupTypeface();
        setupBackgroud();
    }

    public TextViewExtension(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupTypeface();
        setupBackgroud();
    }

    public TextViewExtension(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setupTypeface();
        setupBackgroud();
    }

    private void setupBackgroud() {
        if(getTag().toString().contains("color"))
        {
            //setBackgroundResource(ThemeExtension.getIdColor(getContext(),indexSelectedSettingBackground));
            setClickable(true);
        }
    }

    private void setupTypeface(){
        //setTypefaceSelected(indexSelectedSettingFont);
    }

    public  void setTypefaceSelected(int index){
       // ItemTypeface itemTypeface = getTypefaceSelected(getContext(),index,getTag().toString());
       // this.setTextSize(itemTypeface.getSize());
       // this.setTypeface(itemTypeface.getTypeface());
    }

}
