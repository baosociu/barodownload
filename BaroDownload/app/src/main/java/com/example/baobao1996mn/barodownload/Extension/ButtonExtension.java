package com.example.baobao1996mn.barodownload.Extension;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.baobao1996mn.barodownload.Fragment.FeedbackFragment;
import com.example.baobao1996mn.barodownload.Fragment.HelpFragment;
import com.example.baobao1996mn.barodownload.Fragment.LibraryFragment;
import com.example.baobao1996mn.barodownload.Fragment.SearchFragment;
import com.example.baobao1996mn.barodownload.R;
import com.example.baobao1996mn.barodownload.Utilities.Global;

/**
 * Created by baobao1996mn on 31/08/2017.
 */

public class ButtonExtension extends  TextView {

    public ButtonExtension(Context context) {
        super(context);
        setupTypeface();
        setupLayout();
    }

    public ButtonExtension(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setupTypeface();
        setupLayout();
    }

    public ButtonExtension(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupTypeface();
        setupLayout();
    }

    public void setupLayout() {
        int id = R.color.barColorWhenSelectedDownload;
        if (Global.currentFragment != null) {
            if (Global.currentFragment instanceof FeedbackFragment)
                id = R.color.barColorWhenSelectedFeedback;
            if (Global.currentFragment instanceof HelpFragment)
                id = R.color.barColorWhenSelectedHelp;
            if (Global.currentFragment instanceof LibraryFragment)
                id = R.color.barColorWhenSelectedLibrary;
            if (Global.currentFragment instanceof SearchFragment)
                id = R.color.barColorWhenSelectedSearch;
        }
        setBackgroundColor(getResources().getColor(id));
        setTextColor(Color.WHITE);
        setPadding(20,10,20,10);

    }

    private void setupTypeface(){
        //setTypefaceSelected(indexSelectedSettingFont);
    }

    public  void setTypefaceSelected(int index){
       // ItemTypeface itemTypeface = getTypefaceSelected(getContext(),index,getTag().toString());
       // this.setTextSize(itemTypeface.getSize());
       // this.setTypeface(itemTypeface.getTypeface());
    }

}
