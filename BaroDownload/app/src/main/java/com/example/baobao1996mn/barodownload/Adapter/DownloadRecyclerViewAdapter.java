package com.example.baobao1996mn.barodownload.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.baobao1996mn.barodownload.Extension.ButtonDownloadExtension;
import com.example.baobao1996mn.barodownload.Extension.TextViewExtension;
import com.example.baobao1996mn.barodownload.Model.LinkDownload;
import com.example.baobao1996mn.barodownload.Model.TotalLinkDownload;
import com.example.baobao1996mn.barodownload.R;

/**
 * Created by baobao1996mn on 31/08/2017.
 */

public class DownloadRecyclerViewAdapter extends RecyclerView.Adapter<DownloadRecyclerViewAdapter.ViewHolder>{
    private TotalLinkDownload data;

    public DownloadRecyclerViewAdapter(TotalLinkDownload data) {
        this.data = data;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recyclerview_download,parent,false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LinkDownload linkDownload = data.getLinkDownloadAt(position);
        String info = linkDownload.getDescription()+ "/"+linkDownload.getDataType();
        String title = linkDownload.getTitle();
        holder.download.setLinkDownload(linkDownload);
        holder.download.setText("Download "+info+" ("+linkDownload.getLen()+")");
        holder.title.setText((position+1)+". "+title);
    }

    @Override
    public int getItemCount() {
        return data.getSize();
    }

    static  protected  class ViewHolder extends RecyclerView.ViewHolder{
        public TextViewExtension title;
        public ButtonDownloadExtension download;
        public ViewHolder(View itemView) {
            super(itemView);
            title = (TextViewExtension) itemView.findViewById(R.id.itemDownloadTitle);
            download = (ButtonDownloadExtension) itemView.findViewById(R.id.itemDownloadQualityType);
        }
    }
}
