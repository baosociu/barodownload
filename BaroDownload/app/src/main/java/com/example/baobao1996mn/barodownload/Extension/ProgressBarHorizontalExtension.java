package com.example.baobao1996mn.barodownload.Extension;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import com.example.baobao1996mn.barodownload.Fragment.FeedbackFragment;
import com.example.baobao1996mn.barodownload.Fragment.HelpFragment;
import com.example.baobao1996mn.barodownload.Fragment.LibraryFragment;
import com.example.baobao1996mn.barodownload.Fragment.SearchFragment;
import com.example.baobao1996mn.barodownload.R;
import com.example.baobao1996mn.barodownload.Utilities.Global;

/**
 * Created by baobao1996mn on 06/09/2017.
 */

public class ProgressBarHorizontalExtension extends ProgressBar {
    public ProgressBarHorizontalExtension(Context context) {
        super(context);
        setColorBar();
    }

    public ProgressBarHorizontalExtension(Context context, AttributeSet attrs) {
        super(context, attrs);
        setColorBar();
    }

    public ProgressBarHorizontalExtension(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, android.R.attr.progressBarStyleHorizontal);
        setColorBar();
    }

    public ProgressBarHorizontalExtension(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, android.R.attr.progressBarStyleHorizontal);
        setColorBar();
    }

    private void setColorBar() {
        int id = R.color.barColorWhenSelectedDownload;
        if (Global.currentFragment != null) {
            if (Global.currentFragment instanceof FeedbackFragment)
                id = R.color.barColorWhenSelectedFeedback;
            if (Global.currentFragment instanceof HelpFragment)
                id = R.color.barColorWhenSelectedHelp;
            if (Global.currentFragment instanceof LibraryFragment)
                id = R.color.barColorWhenSelectedLibrary;
            if (Global.currentFragment instanceof SearchFragment)
                id = R.color.barColorWhenSelectedSearch;
        }
        int color = getResources().getColor(id);
        getIndeterminateDrawable().setColorFilter(color, android.graphics.PorterDuff.Mode.MULTIPLY);
    }
}
