package com.example.baobao1996mn.barodownload.Model;

/**
 * Created by baobao1996mn on 31/08/2017.
 */

public class LinkDownload {
    String title;
    String description;
    String dataType;
    String href;
    String len;
    String img;

    public LinkDownload() {
    }

    public LinkDownload(String title, String description, String dataType,  String len,String href, String img) {
        this.title = title;
        this.description = description;
        this.dataType = dataType;
        this.href = href;
        this.len = len;
        this.img = img;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLen() {
        return len;
    }

    public void setLen(String len) {
        this.len = len;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
