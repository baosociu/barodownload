package com.example.baobao1996mn.barodownload.Extension;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by baobao1996mn on 31/08/2017.
 */

public class EditTextExtension extends EditText {

    public EditTextExtension(Context context) {
        super(context);
        setupTypeface();
        setupBackgroud();
    }

    public EditTextExtension(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupTypeface();
        setupBackgroud();
    }

    public EditTextExtension(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupTypeface();
        setupBackgroud();
    }

    public EditTextExtension(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setupTypeface();
        setupBackgroud();
    }

    private void setupBackgroud() {

    }

    private void setupTypeface(){
        //setTypefaceSelected(indexSelectedSettingFont);
    }

    public  void setTypefaceSelected(int index){
       // ItemTypeface itemTypeface = getTypefaceSelected(getContext(),index,getTag().toString());
       // this.setTextSize(itemTypeface.getSize());
       // this.setTypeface(itemTypeface.getTypeface());
    }

}
