package com.example.baobao1996mn.barodownload.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.baobao1996mn.barodownload.AsyncTask.DownloadAsyncTask;
import com.example.baobao1996mn.barodownload.R;
import com.example.baobao1996mn.barodownload.Utilities.Global;
import com.example.baobao1996mn.barodownload.Utilities.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by baobao1996mn on 07/09/2017.
 */

public class DownloadTaskRecyclerViewAdapter extends RecyclerView.Adapter<DownloadTaskRecyclerViewAdapter.ViewHolder> {
    Context context;
    List<ViewHolder> listView;
    public DownloadTaskRecyclerViewAdapter() {
        listView = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context= parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recyclerview_download_tasks,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        listView.add(viewHolder);
        return viewHolder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DownloadAsyncTask task = Global.downloadAsyncTaskList.get(position);
        int color = this.context.getResources().getColor(R.color.barColorWhenSelectedDownload);

        holder.name.setText(task.getFileName());
        holder.status.setText(task.getStringStatus());
        holder.delete.setBackgroundResource(R.drawable.ic_delete_forever);
        holder.line.setBackgroundColor(color);
        holder.progressBar.setMax(100);
        holder.progressBar.setProgress(task.getValueProgress());
        holder.progressBar.getIndeterminateDrawable().setColorFilter(color, android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    @Override
    public int getItemCount() {

        Log.i("DownloadAsynTaskList Size: "+Global.downloadAsyncTaskList.size());
        return Global.downloadAsyncTaskList.size();
    }

    protected static class ViewHolder extends RecyclerView.ViewHolder{
        TextView status;
        TextView name;
        ImageView image, delete;
        ProgressBar progressBar;
        View line;
        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.itemDownloadTaskName);
            status = (TextView) itemView.findViewById(R.id.itemDownloadTaskStatus);
            image = (ImageView) itemView.findViewById(R.id.itemDownloadTaskImage);
            delete = (ImageView) itemView.findViewById(R.id.itemDownloadTaskDelete);
            progressBar = (ProgressBar) itemView.findViewById(R.id.itemDownloadTaskProgress);
        }
    }

    public void refreshProgress(){
        try {
            DownloadAsyncTask task = Global.downloadAsyncTaskList.get(0);
            listView.get(0).status.setText(task.getStringStatus());
            listView.get(0).progressBar.setProgress(0);
        }catch (Exception e){

        }
    }

    public  void updateProgress(String strProgress){
        try {
            DownloadAsyncTask task = Global.downloadAsyncTaskList.get(0);
            listView.get(0).status.setText(strProgress);
            listView.get(0).progressBar.setProgress(task.getValueProgress());
        }catch (Exception e){

        }
    }

    public void completeProgress(){
        try {
            DownloadAsyncTask task = Global.downloadAsyncTaskList.get(0);
            listView.get(0).status.setText(task.getStringStatus());
            listView.remove(0);
            notifyDataSetChanged();
        }catch (Exception e){

        }
    }
}
