package com.example.baobao1996mn.barodownload.Service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;

import com.example.baobao1996mn.barodownload.Activity.MainActivity;
import com.example.baobao1996mn.barodownload.AsyncTask.DownloadAsyncTask;
import com.example.baobao1996mn.barodownload.R;
import com.example.baobao1996mn.barodownload.Utilities.Global;

import static com.example.baobao1996mn.barodownload.AsyncTask.DownloadAsyncTask.COMPLETED_STATUS;
import static com.example.baobao1996mn.barodownload.AsyncTask.DownloadAsyncTask.DOWNLOADING_STATUS;
import static com.example.baobao1996mn.barodownload.AsyncTask.DownloadAsyncTask.WAITING_STATUS;
import static com.example.baobao1996mn.barodownload.Utilities.Global.ID_ASYNC;
import static com.example.baobao1996mn.barodownload.Utilities.Global.completeNotification;

/**
 * Created by baobao1996mn on 04/09/2017.
 */

public class DownloadService extends Service {
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    private boolean isDownloading;
    private boolean isStartingFore;

    @Override
    public void onCreate() {
        // To avoid cpu-blocking, we create a background handler to run our service
        Global.notificationManagerDownload = (NotificationManager) getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(getBaseContext(), MainActivity.class);
        intent.putExtra(Global.OPEN_DOWNLOAD_TASKS,true);
        PendingIntent pendingIntent = PendingIntent.getActivity(getBaseContext(),0,intent,Intent.FLAG_ACTIVITY_NEW_TASK);

        //Global.builderDownload = new NotificationCompat.Builder(getBaseContext());
        Global.builderDownload = new Notification.Builder(getBaseContext());
        Global.builderDownload.setSmallIcon(R.drawable.ic_file_download_white);
        Global.builderDownload.setContentTitle("BaroDownload");
        Global.builderDownload.setContentIntent(pendingIntent);


        //Global.builderDownload.setSubText("...");
        //Global.builderDownload.setProgress(100,0,false);

        isDownloading = false;
        isStartingFore = false;

        HandlerThread thread = new HandlerThread("DownloadService");
        thread.start();

        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, "onStartCommand", Toast.LENGTH_SHORT).show();

        Message message = new Message();
        mServiceHandler.handleMessage(message);

        return START_STICKY;
    }

    protected void downloadTask() {
        Toast.makeText(this, "downloadTask", Toast.LENGTH_SHORT).show();

        //gets the main thread
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (Global.downloadAsyncTaskList.size() != 0) {

                    if(!isStartingFore)
                    {
                        startForeground(ID_ASYNC,Global.builderDownload.build());
                        isStartingFore = true;
                    }

                    DownloadAsyncTask current = Global.downloadAsyncTaskList.get(0);

                    switch (current.getStatusDownload()) {
                        case WAITING_STATUS:
                            try {
                                current.execute();
                                isDownloading = true;
                            } catch (Exception e) {
                            }
                            break;
                        case COMPLETED_STATUS:
                            try {
                                Global.downloadAsyncTaskList.remove(0);
                            }catch (Exception e){

                            }
                            break;
                        case DOWNLOADING_STATUS:
                            break;
                    }
                } else {
                    if (isDownloading) {
                        completeNotification();
                        isDownloading = false;

                        if(isStartingFore)
                        {
                            isStartingFore = false;
                            stopForeground(true);
                            Global.notificationManagerDownload.notify(ID_ASYNC,Global.builderDownload.build());
                        }
                    }
                }
                handler.post(this);
            }
        });
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final class ServiceHandler extends Handler {

        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            downloadTask();
        }
    }

}
