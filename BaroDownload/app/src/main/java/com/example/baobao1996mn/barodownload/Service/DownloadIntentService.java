package com.example.baobao1996mn.barodownload.Service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;

import com.example.baobao1996mn.barodownload.AsyncTask.DownloadAsyncTask;
import com.example.baobao1996mn.barodownload.Utilities.Global;
import com.example.baobao1996mn.barodownload.Utilities.Log;

import java.util.LinkedList;

import static com.example.baobao1996mn.barodownload.AsyncTask.DownloadAsyncTask.COMPLETED_STATUS;
import static com.example.baobao1996mn.barodownload.AsyncTask.DownloadAsyncTask.DOWNLOADING_STATUS;
import static com.example.baobao1996mn.barodownload.AsyncTask.DownloadAsyncTask.WAITING_STATUS;
import static com.example.baobao1996mn.barodownload.Utilities.Global.handlerIntentService;
import static com.example.baobao1996mn.barodownload.Utilities.Global.runnableInentService;

/**
 * Created by baobao1996mn on 01/09/2017.
 */

public class DownloadIntentService extends IntentService {
    public static final int UPDATE_PROGRESS = 8344;


    public DownloadIntentService() {
        super("DownloadIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Global.downloadAsyncTaskList = new LinkedList<DownloadAsyncTask>();
        handlerIntentService = new Handler();
        runnableInentService = new Runnable() {
            @Override
            public void run() {
                Log.i("downloadAsyncTaskList: " + Global.downloadAsyncTaskList.size());
                if (Global.downloadAsyncTaskList.size() != 0) {
                    DownloadAsyncTask current = Global.downloadAsyncTaskList.get(0);

                    switch (current.getStatusDownload()) {
                        case WAITING_STATUS:
                            try {
                                current.execute();
                            } catch (Exception e) {
                            }
                            break;
                        case COMPLETED_STATUS:
                            Global.downloadAsyncTaskList.remove(0);
                            break;
                        case DOWNLOADING_STATUS:
                            break;
                    }
                }
                Global.handlerIntentService.postDelayed(Global.runnableInentService, 0);
            }
        };
        int i = 0;
        while (i == 0) {
            (new DownloadReceiver(new Handler(), getBaseContext())).send(UPDATE_PROGRESS, intent.getBundleExtra("bundleDownload"));
        }
    }

    public static class DownloadReceiver extends ResultReceiver {
        private Context context;

        public DownloadReceiver(Handler handler, Context context) {
            super(handler);
            this.context = context;
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
//            String filename = resultData.getString("filename");
//            String dialogMessage = resultData.getString("dialogMessage");
//            String urlDownload = resultData.getString("urlDownload");
//            Log.i("DownloadAsynceTask: " + filename);
//            DownloadAsyncTask asyncTask = new DownloadAsyncTask(context, filename, dialogMessage, urlDownload);
//            asyncTask.execute();
            Log.i("downloadAsyncTaskList: " + Global.downloadAsyncTaskList.size());
            if (Global.downloadAsyncTaskList.size() != 0) {
                DownloadAsyncTask current = Global.downloadAsyncTaskList.get(0);

                switch (current.getStatusDownload()) {
                    case WAITING_STATUS:
                        try {
                            current.execute();
                        } catch (Exception e) {
                        }
                        break;
                    case COMPLETED_STATUS:
                        Global.downloadAsyncTaskList.remove(0);
                        break;
                    case DOWNLOADING_STATUS:
                        break;
                }
            }

        }
    }
}
