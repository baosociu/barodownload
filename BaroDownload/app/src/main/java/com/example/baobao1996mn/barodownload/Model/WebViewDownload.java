package com.example.baobao1996mn.barodownload.Model;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.baobao1996mn.barodownload.Utilities.Global;

import java.util.ArrayList;
import java.util.List;

import im.delight.android.webview.AdvancedWebView;

/**
 * Created by baobao1996mn on 27/09/2017.
 */

public class WebViewDownload extends AdvancedWebView {
    final String PRE_GETLINK = "PRE_GETLINK";
    final String POST_GETLINK = "POST_GETLINK";
    final String DONE_GETLINK = "DONE_GETLINK";
    String STATUS = PRE_GETLINK;
    String innerHtml = "";
    String title = "";
    String img = "";

    public WebViewDownload(Context context) {
        super(context);
        SetWebClient();
    }

    public WebViewDownload(Context context, AttributeSet attrs) {
        super(context, attrs);
        SetWebClient();
    }

    public WebViewDownload(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        SetWebClient();
    }


    private boolean isPreGetLinkStatus() {
        return STATUS == PRE_GETLINK;
    }

    private boolean isPostGetLinkStatus() {
        return STATUS == POST_GETLINK;
    }

    public boolean isCompletedGetLinkStatus() {
        return STATUS == DONE_GETLINK;
    }

    private void preGetLink() {
        STATUS = PRE_GETLINK;
    }

    private void postGetLink() {
        STATUS = POST_GETLINK;
    }

    private void doneGetLink() {
        STATUS = DONE_GETLINK;
    }

    public void leechLink(String url) {
        if (isPreGetLinkStatus()) {
            img = "";
            title = "";
            innerHtml = "";

            postGetLink();
            //todo input
            String strInput = "document.getElementById(\"video-url\").value = " + "\"" + url + "\"";
            evaluateJavascript(strInput, new ValueCallback<String>() {
                @Override
                public void onReceiveValue(String s) {
                    if (s != null) {
                        //todo click
                        String strCLick = "document.getElementsByClassName(\"btn btn-primary btn-block\")[0].click()";
                        evaluateJavascript(strCLick, null);
                    }
                }
            });
        }
    }

    private void SetWebClient() {
        setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

                if (isPostGetLinkStatus()) {
                    //todo content
                    final String table = "document.getElementById(\"download-list\").innerHTML";
                    final String titleQuery = "document.getElementById(\"video-name\").innerHTML.trim()";
                    final String imgQuery = "document.getElementById(\"video-preview\").getElementsByTagName(\"img\")[0].getAttribute(\"src\")";

                    final Handler handler = new Handler();
                    Runnable runnable;
                    runnable = new Runnable() {
                        @Override
                        public void run() {
                            if (innerHtml == "")
                                evaluateJavascript(table, new ValueCallback<String>() {
                                    @Override
                                    public void onReceiveValue(String s) {
                                        String str = s;
                                        if (str.length() > 100)
                                            innerHtml = str;
                                    }
                                });
                            if(title == "")//todo gettitle
                                evaluateJavascript(titleQuery, new ValueCallback<String>() {
                                    @Override
                                    public void onReceiveValue(String value) {
                                        title = value.replace("amp;", "");
                                        title = value.replace("\"", "");
                                    }
                                });
                            if(img == "")//todo getimage
                                evaluateJavascript(imgQuery, new ValueCallback<String>() {
                                    @Override
                                    public void onReceiveValue(String value) {
                                        img = value.replace("\"", "");
                                    }
                                });
                            if(img != "" && title != "" && innerHtml != "")
                                doneGetLink();
                            if(isCompletedGetLinkStatus())
                                handler.removeCallbacks(this);
                            else
                                handler.post(this);
                        }
                    };
                    handler.post(runnable);
                }
            }
        });
    }

    public TotalLinkDownload getLinkDownload() {
        if (!isCompletedGetLinkStatus() || innerHtml == null)
            return null;

        String s = innerHtml.replace("\\u003C", "");
        s = s.trim();
        List<LinkDownload> listLink = new ArrayList<>();
        List<String> list = new ArrayList<>();
        while (s.contains("td>")) {
            s = s.substring(s.indexOf("td>") + 3);
            String value = s.substring(0, s.indexOf("/td>"));
            if (value.contains("href")) {
                value = value.substring(value.indexOf("href=\\\"") + "href=\\\"".length());
                value = value.substring(0, value.indexOf("\\"));
                value = value.replace("amp;", "");
            }

            list.add(value);
            s = s.substring(s.indexOf("/td>") + 4);
            //*********************
            if (list.size() == 3) {
                String des = list.get(0).split("/")[0];
                String ext = list.get(0).split("/")[1];
                String len = list.get(1);
                String href = list.get(2);
                LinkDownload d = new LinkDownload(title, des, ext, len, Global.host + href, img);
                listLink.add(d);
                list.clear();
            }
            //*********************
        }
        preGetLink();
        return new TotalLinkDownload(listLink);
    }
}
