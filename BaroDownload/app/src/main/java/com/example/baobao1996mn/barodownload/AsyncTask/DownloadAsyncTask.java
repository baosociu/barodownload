package com.example.baobao1996mn.barodownload.AsyncTask;

import android.content.Context;
import android.os.AsyncTask;

import com.example.baobao1996mn.barodownload.R;
import com.example.baobao1996mn.barodownload.Utilities.Global;
import com.example.baobao1996mn.barodownload.Utilities.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by baobao1996mn on 02/09/2017.
 */

public class DownloadAsyncTask extends AsyncTask<String, Integer, String> {
    public static final int WAITING_STATUS = 0;
    public static final int DOWNLOADING_STATUS = 1;
    public static final int COMPLETED_STATUS = -1;

    private String fileName;
    private String url;
    private int status;
    private Context context;
    private int progress;

    public DownloadAsyncTask(Context context, String fileName, String url) {
        this.status = WAITING_STATUS;
        this.fileName = fileName;
        this.url = url;
        this.context = context;
        this.progress = 0;
    }

    @Override
    protected String doInBackground(String... params) {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        HttpURLConnection connection = null;

        try {
            URL url = new URL(this.url);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return "Server returned HTTP " + connection.getResponseCode()
                        + " " + connection.getResponseMessage();
            }

            int fileLength = connection.getContentLength();

            inputStream = connection.getInputStream();
            outputStream = new FileOutputStream("/sdcard/" + this.fileName);

            byte data[] = new byte[4096];
            long total = 0;
            int count;

            //todo: download file
            while ((count = inputStream.read(data)) != -1) {
                if (isCancelled()) {
                    inputStream.close();
                    return null;
                }
                total += count;
                if (fileLength > 0)
                    publishProgress((int) (total * 100 / fileLength), (int) total, (int) fileLength);

                outputStream.write(data, 0, count);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (outputStream != null)
                    outputStream.close();
                if (inputStream != null)
                    inputStream.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.status = DOWNLOADING_STATUS;
        (new Thread(new Runnable() {
            @Override
            public void run() {
                Global.refreshNotification(fileName);
                progress = 0;
            }
        })).start();
    }

    @Override
    protected void onPostExecute(final String s) {
        super.onPostExecute(s);
        try {
            (new Thread(new Runnable() {
                @Override
                public void run() {
                    Global.completeNotification();
                    status = COMPLETED_STATUS;
                    progress = 100;
                }
            })).start();
        } catch (Exception e) {
            Log.i(e.getMessage());
        }
    }


    //
    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        progress = values[0];
        (new Thread(new Runnable() {
            @Override
            public void run() {
                Global.updateNotification(progress);
            }
        })).start();
    }


    public int getStatusDownload() {
        return this.status;
    }

    public String getFileName() {
        return fileName;
    }


    public String getStringStatus() {
        switch (status) {
            case WAITING_STATUS:
                return context.getString(R.string.download_status_waiting);
            case DOWNLOADING_STATUS:
                return context.getString(R.string.download_status_downloading)+" "+progress+"%...";
            case COMPLETED_STATUS:
                return context.getString(R.string.download_status_completed);
        }
        return "";
    }

    public int getValueProgress() {
        return progress;
    }

}
